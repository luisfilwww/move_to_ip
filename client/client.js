// create a socket instance for dashboard
const webDashboardSocket = new WebSocket("ws:movetoip-oroboros.rhcloud.com:8000");

// aux functions
function addLineToTable(data) {
    console.log('data ', data);
    // Id 0 is the dashboard client id so ignore
    if (data.id !== 0) {
        $('#tr-no-connections').remove();
        $('tbody').prepend('<tr id="' + data.id + '">' +
            '<td>' + data.ip + '</td>' +
            '<td>' + data.mac + '</td>' +
            '<td>Yes</td>' +
            '<td>' + new Date().toUTCString() + '</td>' +
            '<td>No</td>' +
            '</tr>');
    }
}

function addLineToLog(data) {
    $('.hostLog').append(data.data)
}

function removeLineFromTable(data) {
    $('#' + data.id).remove();
    console.log('length ', $('tr').length);
    if ($('tr').length === 1) {
        $('tbody').prepend('<tr id="tr-no-connections"> ' +
            '<td colspan="5">No one connected</td> ' +
            '</tr>');
    }
}


//
webDashboardSocket.onopen = function (event) {
    // create the dashboard socket client
    var socketClient = {
        type: 'open',
        ip: null,
        mac: null,
        id: 0,
    };
    // send the socket client obj to the server
    webDashboardSocket.send(JSON.stringify(socketClient));
};

// On socket message
webDashboardSocket.onmessage = function (event) {
    var data = JSON.parse(event.data);
    console.log('data =', data);
    console.log('data.type =', data.type);

    switch (data.type) {
        case 'open':
            addLineToTable(data);
            break;
        case 'remove_client':
            removeLineFromTable(data);
            break;
        case 'log':
            addLineToLog(data);
            break;
        default:
    }
};
