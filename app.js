const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const env = process.env;
const router = require('./router');
const http = require('http');

app.use(express.static('client'));
app.use(bodyParser.json()); // for parsing application/json
app.use('/', router);

// Socket
const server = http.createServer(app);
const createSocket = require('./socket');
createSocket(server);

server.listen(env.NODE_PORT || 3050, env.NODE_IP || '0.0.0.0', function () {
    const port = env.NODE_PORT || 3050;
    const ip = env.NODE_IP || 'localhost';
    console.log('Server with ' + ip + ' listening on port ' + port + '!');

    if (ip === 'localhost') {
        console.log('Running locally:');
        console.log('Do not forget to change the Client socket ip to YOUR_IP:3050');
    }

});
