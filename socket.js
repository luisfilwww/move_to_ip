const WebSocket = require('ws');
const url = require('url');
let CLIENTS = [];
let agentIsRunning = false;

function removeSocketClient(socketClient) {
    console.log('socket client to remove', socketClient);

    for (let i = CLIENTS.length - 1; i >= 0; i--) {
        let tempSocketClient = CLIENTS[i];
        if (tempSocketClient.socketClient.id === socketClient.id) {
            CLIENTS.splice(i, 1);
        }
    }
    console.log('Current Clients ', CLIENTS.length);
    if (CLIENTS.length > 0) {
        CLIENTS[0].send(JSON.stringify({ type: 'remove_client', id: socketClient.id }));
    }
}

function sendMessageToDasboard(data) {
    switch (data.type) {
        case 'open':
            for (let i = 0; i < CLIENTS.length; i++) {
                let socket = CLIENTS[i];
                socket.send(JSON.stringify(data));
            }
            break;
        default:
    }
}

function sendAgentMessageToDashboard(data) {
    let socket = CLIENTS[0];
    socket.send(JSON.stringify(data));
}

function checkAndNotifyAgentToRun(data) {
    if (!agentIsRunning) {
        if (CLIENTS.length > 1) {
            agentIsRunning = true;
            CLIENTS[1].send(JSON.stringify({ type: 'start_agent', id: CLIENTS[1].socketClient.id  }));
        }
    }
}

function getDestination({ criteria, macId }) {
    let out = null;
    const clientsWithoutRequester = CLIENTS.filter((client, index, array) => {
        console.log('client.socketClient.id ', client.socketClient.id);
        console.log('client.socketClient.ip ', client.socketClient.ip);
        if (client.socketClient.id === macId) return false;
        if (client.socketClient.ip === null) return false;
        return true;
    });

    console.log('CLIENTS count', CLIENTS.length);
    console.log('clientsWithoutRequester after:', clientsWithoutRequester.length);

    if (clientsWithoutRequester.length !== 0) {
        for (let i = 0; i < CLIENTS.length; i++) {
            let socket = CLIENTS[i];
            if (socket.socketClient.id === macId) {
                console.log('clientsWithoutRequester.length ', clientsWithoutRequester.length);
                console.log('ramdom e ', Math.floor(Math.random() * clientsWithoutRequester.length));
                const rand = Math.floor(Math.random() * clientsWithoutRequester.length);
                socket.send(JSON.stringify({ type: 'conn_request', targetId: clientsWithoutRequester[rand].socketClient.id }));
            }
        }
    }
}

module.exports = function (server) {
    const wss = new WebSocket.Server({ server });

    wss.on('connection', function connection(ws) {

        ws.on('message', function incoming(message) {
            const messageParsed = JSON.parse(message);

            switch (messageParsed.type) {
                case 'open':
                    ws.socketClient = messageParsed;
                    CLIENTS.push(ws);
                    sendMessageToDasboard(messageParsed);
                    checkAndNotifyAgentToRun(messageParsed);
                    break;
                case 'get_destination':
                    console.log('get_destination ', messageParsed);
                    getDestination(messageParsed);
                    break;
                case 'log':
                    sendAgentMessageToDashboard(messageParsed);
                    break;
                default:
            }
        });

        ws.on('close', function close() {
            console.log('disconnected', ws.socketClient);
            // remove client from socket clients
            removeSocketClient(ws.socketClient);
            // notify dashboard to update
        });
    });
}
